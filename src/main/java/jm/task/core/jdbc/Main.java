package jm.task.core.jdbc;

import jm.task.core.jdbc.service.UserServiceImpl;

public class Main {

    public static void main(String[] args) {
        // Реализуйте алгоритм здесь
        UserServiceImpl userService = new UserServiceImpl();

        userService.createUsersTable();
        userService.saveUser("Dima1", "Orlov1", (byte) 36);
        userService.saveUser("Dima2", "Orlov2", (byte) 36);
        userService.saveUser("Dima3", "Orlov3", (byte) 36);
        userService.saveUser("Dima4", "Orlov4", (byte) 36);
        userService.getAllUsers().forEach(System.out::println);
        userService.removeUserById(2);
        userService.getAllUsers().forEach(System.out::println);
        userService.cleanUsersTable();
        userService.getAllUsers().forEach(System.out::println);
        userService.dropUsersTable();
    }

}
