package jm.task.core.jdbc.dao;

import jm.task.core.jdbc.model.User;
import jm.task.core.jdbc.util.Util;
import org.hibernate.Session;
import org.hibernate.SessionException;
import org.hibernate.SessionFactory;

import java.util.List;

public class UserDaoHibernateImpl implements UserDao {
    public UserDaoHibernateImpl() {
    }


    @Override
    public void createUsersTable() {
        try (Session session = Util.mySession()) {
            session.beginTransaction();
            session.createSQLQuery("CREATE TABLE IF NOT EXISTS katatable" +
                    "(id BIGINT PRIMARY KEY AUTO_INCREMENT, " +
                    "name VARCHAR(255), " +
                    "lastname VARCHAR(255), " +
                    "age TINYINT(150))").addEntity(User.class).executeUpdate();
            session.getTransaction().commit();
            System.out.println("Таблица Создана");
        } catch (SessionException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void dropUsersTable() {
        try (Session session = Util.mySession()) {
            session.beginTransaction();
            session.createSQLQuery("DROP TABLE IF EXISTS katatable")
                    .addEntity(User.class).executeUpdate();
            session.getTransaction().commit();
            System.out.println("Таблица Удалена");
        } catch (SessionException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void saveUser(String name, String lastname, byte age) {
        try (Session session = Util.mySession()) {
            session.beginTransaction();
            session.save(new User(name, lastname, age));
            session.getTransaction().commit();
            System.out.println("Строка добавлена");
        } catch (SessionException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void removeUserById(long id) {
        try (Session session = Util.mySession()) {
            session.beginTransaction();
            session.delete(session.get(User.class, id));
            session.getTransaction().commit();
            System.out.println("Строка удалена");
        } catch (SessionException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<User> getAllUsers() {
        List<User> list = null;
        try (Session session = Util.mySession()) {
            session.beginTransaction();
            list = session.createQuery("from User").getResultList();
            session.getTransaction().commit();
        } catch (SessionException e) {
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public void cleanUsersTable() {
        int num;
        try (Session session = Util.mySession()) {
            session.beginTransaction();
            num = session.createQuery("delete User").executeUpdate();
            session.getTransaction().commit();
            System.out.println("Удалено строк " + num);
        } catch (SessionException e) {
            e.printStackTrace();
        }
    }
}
