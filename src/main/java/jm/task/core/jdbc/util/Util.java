package jm.task.core.jdbc.util;

import jm.task.core.jdbc.model.User;
import org.hibernate.Session;
import org.hibernate.SessionException;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.Scanner;

public class Util {
    // реализуйте настройку соеденения с БД
    private final static String URL = initURL();
    private final static String NAME = initName();
    private final static String PASSWORD = initPassword();
    private static SessionFactory sessionFactory = null;
    private static Connection connection = null;


    public static Connection getConnectionMy() {
        try {
            connection = DriverManager.getConnection(URL, NAME, PASSWORD);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return connection;
    }

    public static Session mySession() {
        Session session = null;

        try {
            Properties properties = new Properties();
            properties.setProperty("hibernate.connection.url", URL);
            properties.setProperty("hibernate.connection.username", NAME);
            properties.setProperty("hibernate.connection.password", PASSWORD);
            properties.setProperty("dialect", "org.hibernate.dialect.MySQLDialect");
            properties.setProperty("hibernate.connection.driver_class", "com.mysql.cj.jdbc.Driver");

            if (sessionFactory == null) {
                sessionFactory = new Configuration()
                        .addProperties(properties)
                        .addAnnotatedClass(User.class)
                        .buildSessionFactory();
            }
            session = sessionFactory.openSession();

        } catch (SessionException e) {
            e.printStackTrace();
        }
        return session;
    }

    private static String initURL() {
        System.out.println("Введите URL");
        return reader();
    }

    private static String initName() {
        System.out.println("Введите имя");
        return reader();
    }

    private static String initPassword() {
        System.out.println("Введите пароль");
        return reader();
    }

    private static String reader() {
        Scanner scanner = new Scanner(System.in);
        return scanner.nextLine();
    }

}
